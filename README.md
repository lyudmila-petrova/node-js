Тестовое задание на вакансию «back-end разработчик»
===

На базе [adonis.js](https://adonisjs.com/) разработать микросервис управления «ресурсами». 

[Спецификация OpenApi 3.0](openapi.yml)

**Требования:**

* Разработать микросервис управления ресурсами на базе [adonis.js](https://adonisjs.com/) опираясь на [Спецификацию](openapi.yml);
* DB: Postgres или MongoDB;
* Event-drive на [NATS MQ](https://nats.io/) Аналогично REST сервиса реализовать event-drive;

**Опционально:**
* Завернуть в докер
* тесты

Результат работы можете предоставить в любом удобном для Вас виде (например прислать на [bad@alt-point.com](mailto:bad@alt-point.com)), но лучше через merge-request к этому репозиторию;

