'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ResourceSchema extends Schema {
  up () {
    this.raw(
      `
        DROP TYPE IF EXISTS resource_type;
        DROP TYPE IF EXISTS resource_presentation_type;
      `
    );

    //this.dropIfExists('resource_type');

    //this.dropIfExists('resource_presentation_type');

    this.create('resources', (table) => {
      table.uuid('id').primary()
      table.string('description').nullable().comment("Описание содержимого файла")
      table.string('name').nullable().comment("Название файла для фронта")
      table.string('path').nullable().comment("Путь к файлу в файловой системе")
      table.binary('file').nullable()
      table.enu('type', ['file', 'link'], { useNative: true, enumName: 'resource_type'}).defaultTo('file')
      table.string('target').nullable().comment("URI для type=link")
      table.json('meta').comment("mime и size для type=file")
      table.enu('presentation_type', ['image', 'video', 'text', 'archive', 'pdf'], { useNative: true, enumName: 'resource_presentation_type'})
      table.timestamps()
    })
  }

  down () {
    this.drop('resources')
  }
}

module.exports = ResourceSchema
