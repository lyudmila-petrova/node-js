'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RemoveFileColumnFromResourcesSchema extends Schema {
  up () {
    this.table('resources', (table) => {
      table.dropColumn('file');
    })
  }

  down () {
    this.table('resources', (table) => {
      table.binary('file').nullable()
    })
  }
}

module.exports = RemoveFileColumnFromResourcesSchema
