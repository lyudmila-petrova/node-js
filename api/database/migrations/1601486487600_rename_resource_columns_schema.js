'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RenameResourceColumnsSchema extends Schema {
  up () {
    this.table('resources', (table) => {
      table.renameColumn('created_at', 'createdAt')
      table.renameColumn('updated_at', 'updatedAt')
      table.renameColumn('presentation_type', 'presentationType')
    })
  }

  down () {
    this.table('resources', (table) => {
      table.renameColumn('createdAt', 'created_at')
      table.renameColumn('updatedAt', 'updated_at')
      table.renameColumn('presentationType', 'presentation_type')
    })
  }
}

module.exports = RenameResourceColumnsSchema
