'use strict'

const { test, trait } = use('Test/Suite')('Resource Creation')

trait('Test/ApiClient')

const Drive = use('Drive')

const fs = require('fs');
const path = require('path')
const mime = require('mime-types')

const TEST_FILES_DIR = Drive.disk('test_src').driver.root;

const get_headers = (file_path) => {
  const stat = fs.statSync(file_path)
  return {
    size: stat.size,
    content_type: mime.lookup(file_path)
  }
}

const SUNFLOWER = {
  id: 'c29e1580-e387-4710-a3a2-06b008647e87',
  description: 'Картинка с подсолнухом',
  name: 'sunflower.jpeg',
  path: 'some/relative/path',
  type: 'file',
  presentationType: 'image',
}

test('sunflower upload ok', async ({ client }) => {

  const file_name = 'sunflower.jpeg'

  const file_path = path.join(TEST_FILES_DIR, file_name)

  const exists = await Drive.disk('test_src').exists('sunflower.jpeg');

  const data = Object.assign({}, SUNFLOWER)
  data.mime = await get_headers(file_path)

  const response = await client
    .post('/')
    .field('id', data.id)
    .field('description', data.description)
    .field('name', data.name)
    .field('path', data.path)
    .field('type', data.type)
    .field('presentationType', data.presentationType)
    .attach('file', file_path)
    .field('meta', JSON.stringify(data.mime))
    .end()

  if (response.status !== 200) {
    console.log(response.body)
  }

  response.assertStatus(200)

  response.assertJSONSubset(SUNFLOWER)
})

test('sunflower remove ok', async ({ client }) => {
  const response = await client
    .delete('/'+SUNFLOWER.id)
    .end()

  if (response.status !== 200) {
    console.log(response.body)
  }

  response.assertStatus(200)
})
