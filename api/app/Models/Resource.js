'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Resource extends Model {
  static get table () {
    return 'resources'
  }

  static get primaryKey () {
    return 'id'
  }

  static get createdAtColumn () {
    return 'createdAt'
  }

  static get updatedAtColumn () {
    return 'updatedAt'
  }

  static get incrementing () {
    return false
  }

}

module.exports = Resource
