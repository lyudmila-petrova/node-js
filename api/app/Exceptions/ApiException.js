'use strict'

class ApiException extends Error {
  constructor (message, key, code) {
    super(message)

    // extending Error is weird and does not propagate `message`
    Object.defineProperty(this, 'message', {
      configurable: true,
      enumerable: true,
      value: message,
      writable: true
    })

    Object.defineProperty(this, 'name', {
      configurable: true,
      enumerable: false,
      value: this.constructor.name,
      writable: true
    })

    Object.defineProperty(this, 'key', {
      configurable: true,
      enumerable: false,
      value: key || 0,
      writable: true
    })

    Object.defineProperty(this, 'code', {
      configurable: true,
      enumerable: false,
      value: code,
      writable: true
    })

    if (Error.hasOwnProperty('captureStackTrace')) {
      Error.captureStackTrace(this, this.constructor)
      return
    }

    Object.defineProperty(this, 'stack', {
      configurable: true,
      enumerable: false,
      value: (new Error(message)).stack,
      writable: true
    })
  }
}

module.exports = ApiException
