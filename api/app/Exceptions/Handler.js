'use strict'

const BaseExceptionHandler = use('BaseExceptionHandler')

class ExceptionHandler extends BaseExceptionHandler {

  async handle (error, { request, response }) {
    response.status(400).json({
      code: error.code,
      key: error.key || 0,
      message: error.message,
    })
    //return super.handle(...arguments)
  }
}

module.exports = ExceptionHandler
