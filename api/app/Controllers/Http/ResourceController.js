'use strict'

const Resource = use('App/Models/Resource')

const Database = use('Database')

const { rule } = require('indicative')
const { validateAll } = use('Validator')

const path = require('path')
const Drive = use('Drive')
const fs = require('fs');

const FILES_DIR = Drive.disk().driver.root

const ApiException = use("App/Exceptions/ApiException")

class ResourceController {

  async search ({request, response}) {
    const get = request.get()

    const rules = {
      page: 'integer|min:1',
      perPage: 'integer|min:1',
      sort_by: 'in:createdAt,updatedAt,name',
      sort_dir: 'in:asc,desc',
      query: [rule('regex','^[a-z]+$')], //TODO: fix regex stub
    }

    const validation = await validateAll(get, rules)
    if (validation.fails()) {
      throw new ApiException(
        validation.messages(),
        0,
        'SEARCH_PARAMS_NOT_VALID'
      )
    }

    const page = request.get().page || 1
    const perPage = request.get().limit || 10

    const sort_by = request.get().sortBy || 'created_at'
    const sort_dir = request.get().sortDir || 'desc'

    const like_pattern = request.get().query

    let query = Database
      .from('resources')

    if (like_pattern) {
      query = query.whereRaw(`'name' LIKE '%${like_pattern}%'`)
    }

    query
      .orderBy(sort_by, sort_dir)
      .paginate(page, perPage)

    let resources = await query

    return response.json(resources)
  }

  async save ({request, response}) {
      const commonRules = {
        id: 'required',
        description: 'string:allowNull',
        type: 'in:file,link',
        presentationType: 'in:image,video,text,archive,pdf',
      }

      const fileRules = {
        meta: 'required',
        target: [rule('equals', null)],
      }

      const linkRules = {
        meta: [rule('equals', null)],
        target: 'required',
      }

      const resourceData = request.only([
        'id', 'description', 'name', 'path',
        'type', 'target', 'meta', 'presentationType',
      ]);

      const rules = resourceData.type === 'file'
        ? {...commonRules, ...fileRules}
        : {...commonRules, ...linkRules};

      const validation = await validateAll(resourceData, rules)
      if (validation.fails()) {
        throw new ApiException(
          validation.messages(),
          0,
          'RESOURCE_NOT_VALID'
        )
      }

      if (resourceData.type === 'file') {
        const file = request.file('file', {})

        if(!resourceData.name) {
          resourceData.name = file.clientName
        }

        const relative_local_file_path = resourceData.path || ''

        await file.move(path.join(FILES_DIR, relative_local_file_path), {
          overwrite: true,
          name: resourceData.name
        })

        if (!file.moved()) {
          throw new ApiException(
            resourceData.file.error(),
            0,
            'FILE_NOT_MOVED'
          )
        }
      }

      const r = await Resource.create(resourceData)

      return response.status(200).json(r)
  }

  async show ({params, response}) {
    const r = await Resource.find(params.id)

    return response.json(r)
  }

  async delete ({params, response}) {
    // TODO: обработка исключений + стандартный формат ошибки
    const r = await Resource.find(params.id)
    if (!r) {
      return response.status(404).json({data: 'Resource not found'})
    }
    const file_path = path.join(FILES_DIR, r.path, r.name)

    await r.delete()
    fs.unlinkSync(file_path)

    return response.status(200).json({})
  }
}

module.exports = ResourceController
