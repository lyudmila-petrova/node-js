'use strict'

const Helpers = use('Helpers')
const Env = use('Env')

const path = require('path')

module.exports = {
  /*
  |--------------------------------------------------------------------------
  | Default disk
  |--------------------------------------------------------------------------
  |
  | The default disk is used when you interact with the file system without
  | defining a disk name
  |
  */
  default: 'local',

  disks: {
    local: {
      root: path.join(Helpers.appRoot(), 'files'),
      driver: 'local'
    },

    test_src: {
      root: path.join(Helpers.appRoot(), 'test/files'),
      driver: 'local'
    },

    s3: {
      driver: 's3',
      key: Env.get('S3_KEY'),
      secret: Env.get('S3_SECRET'),
      bucket: Env.get('S3_BUCKET'),
      region: Env.get('S3_REGION')
    }
  }
}
